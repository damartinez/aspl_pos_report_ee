# -*- coding: utf-8 -*-
#################################################################################
# Author      : Acespritech Solutions Pvt. Ltd. (<www.acespritech.com>)
# Copyright(c): 2012-Present Acespritech Solutions Pvt. Ltd.
# All Rights Reserved.
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#################################################################################

import base64
import io
from collections import defaultdict
from datetime import datetime

import pytz
import xlwt
from odoo import fields, models, _


class EmployeeHourlySale(models.TransientModel):
    _name = 'employee.hourly.sale'
    _description = 'Employee Hourly Sale'

    date = fields.Date('Date', default=fields.Date.context_today)
    data = fields.Binary(string="Data")
    name = fields.Char(string="name")
    detail_report = fields.Boolean(string="Detailed Report")
    file_download = fields.Boolean(string="Download")

    def employee_sales_xls_report(self):
        font = xlwt.Font()
        workbook = xlwt.Workbook(encoding="utf-8")
        worksheet = workbook.add_sheet(_("POS Report"))
        font_p = xlwt.Font()
        font_p.bold = True
        alignment = xlwt.Alignment()
        style_border = xlwt.XFStyle()
        style_border.font = font_p
        style_border.alignment = alignment
        borders = xlwt.Borders()
        style_border.borders = borders

        row = 5
        col = 3

        num1 = 5

        num2 = 3
        num3 = 5
        date_display = datetime.strptime(str(self.date), "%Y-%m-%d").strftime('%d-%b,%Y')
        worksheet.write(1, 0, _("Date"), style=xlwt.easyxf('font: bold on, color black;'))
        worksheet.write(1, 1, str(date_display), style=xlwt.easyxf('font: bold on, color black;'))
        header = ['1 AM-2 AM', '2 AM-3 AM', '3 AM-4 AM', '4 AM-5 AM', '5 AM-6 AM', '6 AM-7 AM',
                  '7 AM-8 AM',
                  '8 AM-9 AM', '9 AM-10 AM', '10 AM-11 AM', '11 AM-12 PM', '12 PM-1 PM',
                  '1 PM-2 PM', '2 PM-3 PM',
                  '3 PM-4 PM', '4 PM-5 PM', '5 PM-6 PM', '6 PM-7 PM', '7 PM-8 PM', '8 PM-9 PM',
                  '9 PM-10 PM',
                  '10 PM-11 PM', '11 PM-12 AM', '12 AM-1 AM']
        sub_header = [_('Orders'), _('Amount'), _('Quantity')]
        worksheet.write_merge(3, 3, 0, 2, _('Hour'), style=xlwt.easyxf(
            'font: bold on, color black; '
            'borders: top_color black, bottom_color black, '
            'right_color black, left_color black, left thin,'
            ' right thin, top thin, bottom thin; pattern: pattern solid, fore_color white;'))
        worksheet.write_merge(4, 4, 0, 2, _('Sales Person'), style=xlwt.easyxf(
            'font: bold on, color black;'
            ' borders: top_color black, bottom_color black, right_color black, '
            'left_color black, left thin, right thin, top thin, bottom thin;'
            ' pattern: pattern solid, fore_color white;'))

        for num in header[0:len(header)]:
            worksheet.write_merge(3, 3, num2, num3, num, style=xlwt.easyxf(
                'font: bold on, color black;'
                'borders: top_color black, bottom_color black, right_color black, '
                'left_color black,'
                ' left thin, right thin, top thin, bottom thin;'
                ' pattern: pattern solid, fore_color white;align: horiz center;'))
            for sub in sub_header[0:len(sub_header)]:
                worksheet.write_merge(4, 4, col, col, sub, style=xlwt.easyxf(
                    'font: bold on, color black;'
                    ' borders: top_color black, bottom_color black, '
                    'right_color black, left_color black, left thin, right thin, '
                    'top thin, bottom thin; pattern: pattern solid, fore_color white;'))
                col += 1
            num2 += 3
            num3 += 3
        user_id = self.env['res.users'].search_read([], ['name'])
        if not self.detail_report:
            for user in user_id:
                worksheet.write_merge(row, row, 0, 2, user.get('name'))
                for num in range(24):
                    self_date = str(
                        datetime.strptime(str(self.date), "%Y-%m-%d").date()) + ' ' + str(
                        num) + ':00:00'
                    self_date1 = str(
                        datetime.strptime(str(self.date), "%Y-%m-%d").date()) + ' ' + str(
                        num) + ':59:59'
                    order_id = self.env['pos.order'].search(
                        [('user_id', '=', user.get('id')), ('date_order', '>=', self_date),
                         ('date_order', '<=', self_date1)])
                    order_count = len(order_id)
                    order_amount = sum(order_id.mapped('amount_total'))
                    products_qty = sum(order_id.mapped('lines').mapped('qty'))
                    products = float(products_qty)
                    order_dates = order_id.mapped('date_order')
                    tz_date_time = [date_time_tz.replace(tzinfo=pytz.UTC).astimezone(
                        pytz.timezone(self.env.user.tz))
                        for date_time_tz in order_dates]
                    hour_list_tz = [order_hour.hour for order_hour in tz_date_time]
                    if hour_list_tz and hour_list_tz[0] and hour_list_tz[0] == 1:
                        worksheet.write(row, 3, order_count)
                        worksheet.write(row, 4, order_amount)
                        worksheet.write(row, 5, products)

                    if hour_list_tz and hour_list_tz[0] and hour_list_tz[0] == 2:
                        worksheet.write(row, 6, order_count)
                        worksheet.write(row, 7, order_amount)
                        worksheet.write(row, 8, products)

                    if hour_list_tz and hour_list_tz[0] and hour_list_tz[0] == 3:
                        worksheet.write(row, 9, order_count)
                        worksheet.write(row, 10, order_amount)
                        worksheet.write(row, 11, products)

                    if hour_list_tz and hour_list_tz[0] and hour_list_tz[0] == 4:
                        worksheet.write(row, 12, order_count)
                        worksheet.write(row, 13, order_amount)
                        worksheet.write(row, 14, products)

                    if hour_list_tz and hour_list_tz[0] and hour_list_tz[0] == 5:
                        worksheet.write(row, 15, order_count)
                        worksheet.write(row, 16, order_amount)
                        worksheet.write(row, 17, products)

                    if hour_list_tz and hour_list_tz[0] and hour_list_tz[0] == 6:
                        worksheet.write(row, 18, order_count)
                        worksheet.write(row, 19, order_amount)
                        worksheet.write(row, 20, products)

                    if hour_list_tz and hour_list_tz[0] and hour_list_tz[0] == 7:
                        worksheet.write(row, 21, order_count)
                        worksheet.write(row, 22, order_amount)
                        worksheet.write(row, 23, products)

                    if hour_list_tz and hour_list_tz[0] and hour_list_tz[0] == 8:
                        worksheet.write(row, 24, order_count)
                        worksheet.write(row, 25, order_amount)
                        worksheet.write(row, 26, products)

                    if hour_list_tz and hour_list_tz[0] and hour_list_tz[0] == 9:
                        worksheet.write(row, 27, order_count)
                        worksheet.write(row, 28, order_amount)
                        worksheet.write(row, 29, products)

                    if hour_list_tz and hour_list_tz[0] and hour_list_tz[0] == 10:
                        worksheet.write(row, 30, order_count)
                        worksheet.write(row, 31, order_amount)
                        worksheet.write(row, 32, products)

                    if hour_list_tz and hour_list_tz[0] and hour_list_tz[0] == 11:
                        worksheet.write(row, 33, order_count)
                        worksheet.write(row, 34, order_amount)
                        worksheet.write(row, 35, products)

                    if hour_list_tz and hour_list_tz[0] and hour_list_tz[0] == 12:
                        worksheet.write(row, 36, order_count)
                        worksheet.write(row, 37, order_amount)
                        worksheet.write(row, 38, products)

                    if hour_list_tz and hour_list_tz[0] and hour_list_tz[0] == 13:
                        worksheet.write(row, 39, order_count)
                        worksheet.write(row, 40, order_amount)
                        worksheet.write(row, 41, products)

                    if hour_list_tz and hour_list_tz[0] and hour_list_tz[0] == 14:
                        worksheet.write(row, 42, order_count)
                        worksheet.write(row, 43, order_amount)
                        worksheet.write(row, 44, products)

                    if hour_list_tz and hour_list_tz[0] and hour_list_tz[0] == 15:
                        worksheet.write(row, 45, order_count)
                        worksheet.write(row, 46, order_amount)
                        worksheet.write(row, 47, products)

                    if hour_list_tz and hour_list_tz[0] and hour_list_tz[0] == 16:
                        worksheet.write(row, 48, order_count)
                        worksheet.write(row, 49, order_amount)
                        worksheet.write(row, 50, products)

                    if hour_list_tz and hour_list_tz[0] and hour_list_tz[0] == 17:
                        worksheet.write(row, 51, order_count)
                        worksheet.write(row, 52, order_amount)
                        worksheet.write(row, 53, products)

                    if hour_list_tz and hour_list_tz[0] and hour_list_tz[0] == 18:
                        worksheet.write(row, 54, order_count)
                        worksheet.write(row, 55, order_amount)
                        worksheet.write(row, 56, products)

                    if hour_list_tz and hour_list_tz[0] and hour_list_tz[0] == 19:
                        worksheet.write(row, 57, order_count)
                        worksheet.write(row, 58, order_amount)
                        worksheet.write(row, 59, products)

                    if hour_list_tz and hour_list_tz[0] and hour_list_tz[0] == 20:
                        worksheet.write(row, 60, order_count)
                        worksheet.write(row, 61, order_amount)
                        worksheet.write(row, 62, products)

                    if hour_list_tz and hour_list_tz[0] and hour_list_tz[0] == 21:
                        worksheet.write(row, 63, order_count)
                        worksheet.write(row, 64, order_amount)
                        worksheet.write(row, 65, products)

                    if hour_list_tz and hour_list_tz[0] and hour_list_tz[0] == 22:
                        worksheet.write(row, 66, order_count)
                        worksheet.write(row, 67, order_amount)
                        worksheet.write(row, 68, products)

                    if hour_list_tz and hour_list_tz[0] and hour_list_tz[0] == 23:
                        worksheet.write(row, 69, order_count)
                        worksheet.write(row, 70, order_amount)
                        worksheet.write(row, 71, products)

                    if hour_list_tz and hour_list_tz[0] and hour_list_tz[0] == 24:
                        worksheet.write(row, 72, order_count)
                        worksheet.write(row, 73, order_amount)
                        worksheet.write(row, 74, products)
                    if hour_list_tz and hour_list_tz[0] and hour_list_tz[0] == 0:
                        worksheet.write(row, 75, order_count)
                        worksheet.write(row, 76, order_amount)
                        worksheet.write(row, 77, products)

                row += 1

            users = {}
        else:
            users = {}
            for user in user_id:
                order_ids = self.env['pos.order'].search(
                    [('user_id', '=', user.get('id')), ('date_order', '>=', self.date),
                     ('date_order', '<=', self.date)])
                users[user.get('name')] = []
                for order_id in order_ids:
                    hour = {}
                    if order_id:
                        date = datetime.strptime(str(order_id.date_order), "%Y-%m-%d  %H:%M:%S")
                        current_tz = date.replace(tzinfo=pytz.UTC).astimezone(
                            pytz.timezone(self.env.user.tz))
                        hour[current_tz.hour] = [order_id.name, order_id.amount_total,
                                                 sum(order_id.mapped('lines').mapped('qty'))]
                    users[user.get('name')].append(hour)
                user_dict = defaultdict(list)
                for rec in users[user.get('name')]:
                    # you can list as many input dicts as you want here
                    for key, value in rec.items():
                        user_dict[key].append(value)
                users[user.get('name')] = user_dict
            for user_record in users:
                worksheet.write_merge(row, row, 0, 2, user_record)
                for num in range(24):
                    if users[user_record][num + 1]:
                        if num + 1 == 1:
                            row1 = num1
                            for record in users[user_record][num + 1]:
                                worksheet.write(row1, 3, record[0])
                                worksheet.write(row1, 4, record[1])
                                worksheet.write(row1, 5, record[2])
                                row1 += 1
                        if num + 1 == 2:
                            row1 = num1
                            for record in users[user_record][num + 1]:
                                worksheet.write(row1, 6, record[0])
                                worksheet.write(row1, 7, record[1])
                                worksheet.write(row1, 8, record[2])
                                row1 += 1
                        if num + 1 == 3:
                            row1 = num1
                            for record in users[user_record][num + 1]:
                                worksheet.write(row1, 9, record[0])
                                worksheet.write(row1, 10, record[1])
                                worksheet.write(row1, 11, record[2])
                                row1 += 1
                        if num + 1 == 4:
                            row1 = num1
                            for record in users[user_record][num + 1]:
                                worksheet.write(row1, 12, record[0])
                                worksheet.write(row1, 13, record[1])
                                worksheet.write(row1, 14, record[2])
                                row1 += 1
                        if num + 1 == 5:
                            row1 = num1
                            for record in users[user_record][num + 1]:
                                worksheet.write(row1, 15, record[0])
                                worksheet.write(row1, 16, record[1])
                                worksheet.write(row1, 17, record[2])
                                row1 += 1
                        if num + 1 == 6:
                            row1 = num1
                            for record in users[user_record][num + 1]:
                                worksheet.write(row1, 18, record[0])
                                worksheet.write(row1, 19, record[1])
                                worksheet.write(row1, 20, record[2])
                                row1 += 1
                        if num + 1 == 7:
                            row1 = num1
                            for record in users[user_record][num + 1]:
                                worksheet.write(row1, 21, record[0])
                                worksheet.write(row1, 22, record[1])
                                worksheet.write(row1, 23, record[2])
                                row1 += 1
                        if num + 1 == 8:
                            row1 = num1
                            for record in users[user_record][num + 1]:
                                worksheet.write(row1, 24, record[0])
                                worksheet.write(row1, 25, record[1])
                                worksheet.write(row1, 26, record[2])
                                row1 += 1
                        if num + 1 == 9:
                            row1 = num1
                            for record in users[user_record][num + 1]:
                                worksheet.write(row1, 27, record[0])
                                worksheet.write(row1, 28, record[1])
                                worksheet.write(row1, 29, record[2])
                                row1 += 1
                        if num + 1 == 10:
                            row1 = num1
                            for record in users[user_record][num + 1]:
                                worksheet.write(row1, 30, record[0])
                                worksheet.write(row1, 31, record[1])
                                worksheet.write(row1, 32, record[2])
                                row1 += 1
                        if num + 1 == 11:
                            row1 = num1
                            for record in users[user_record][num + 1]:
                                worksheet.write(row1, 33, record[0])
                                worksheet.write(row1, 34, record[1])
                                worksheet.write(row1, 35, record[2])
                                row1 += 1
                        if num + 1 == 12:
                            row1 = num1
                            for record in users[user_record][num + 1]:
                                worksheet.write(row1, 36, record[0])
                                worksheet.write(row1, 37, record[1])
                                worksheet.write(row1, 38, record[2])
                                row1 += 1
                        if num + 1 == 13:
                            row1 = num1
                            for record in users[user_record][num + 1]:
                                worksheet.write(row1, 39, record[0])
                                worksheet.write(row1, 40, record[1])
                                worksheet.write(row1, 41, record[2])
                                row1 += 1
                        if num + 1 == 14:
                            row1 = num1
                            for record in users[user_record][num + 1]:
                                worksheet.write(row1, 42, record[0])
                                worksheet.write(row1, 43, record[1])
                                worksheet.write(row1, 44, record[2])
                                row1 += 1
                        if num + 1 == 15:
                            row1 = num1
                            for record in users[user_record][num + 1]:
                                worksheet.write(row1, 45, record[0])
                                worksheet.write(row1, 46, record[1])
                                worksheet.write(row1, 47, record[2])
                                row1 += 1
                        if num + 1 == 16:
                            row1 = num1
                            for record in users[user_record][num + 1]:
                                worksheet.write(row1, 48, record[0])
                                worksheet.write(row1, 49, record[1])
                                worksheet.write(row1, 50, record[2])
                                row1 += 1
                        if num + 1 == 17:
                            row1 = num1
                            for record in users[user_record][num + 1]:
                                worksheet.write(row1, 51, record[0])
                                worksheet.write(row1, 52, record[1])
                                worksheet.write(row1, 53, record[2])
                                row1 += 1
                        if num + 1 == 18:
                            row1 = num1
                            for record in users[user_record][num + 1]:
                                worksheet.write(row1, 54, record[0])
                                worksheet.write(row1, 55, record[1])
                                worksheet.write(row1, 56, record[2])
                                row1 += 1
                        if num + 1 == 19:
                            row1 = num1
                            for record in users[user_record][num + 1]:
                                worksheet.write(row1, 57, record[0])
                                worksheet.write(row1, 58, record[1])
                                worksheet.write(row1, 59, record[2])
                                row1 += 1
                        if num + 1 == 20:
                            row1 = num1
                            for record in users[user_record][num + 1]:
                                worksheet.write(row1, 60, record[0])
                                worksheet.write(row1, 61, record[1])
                                worksheet.write(row1, 62, record[2])
                                row1 += 1
                        if num + 1 == 21:
                            row1 = num1
                            for record in users[user_record][num + 1]:
                                worksheet.write(row1, 63, record[0])
                                worksheet.write(row1, 64, record[1])
                                worksheet.write(row1, 65, record[2])
                                row1 += 1
                        if num + 1 == 22:
                            row1 = num1
                            for record in users[user_record][num + 1]:
                                worksheet.write(row1, 66, record[0])
                                worksheet.write(row1, 67, record[1])
                                worksheet.write(row1, 68, record[2])
                                row1 += 1
                        if num + 1 == 23:
                            row1 = num1
                            for record in users[user_record][num + 1]:
                                worksheet.write(row1, 69, record[0])
                                worksheet.write(row1, 70, record[1])
                                worksheet.write(row1, 71, record[2])
                                row1 += 1
                        if num + 1 == 24:
                            row1 = num1
                            for record in users[user_record][num + 1]:
                                worksheet.write(row1, 72, record[0])
                                worksheet.write(row1, 73, record[1])
                                worksheet.write(row1, 74, record[2])
                                row1 += 1
                        if num + 1 == 0:
                            row1 = num1
                            for record in users[user_record][num + 1]:
                                worksheet.write(row1, 75, record[0])
                                worksheet.write(row1, 76, record[1])
                                worksheet.write(row1, 77, record[2])
                                row1 += 1
                        row += 1
                row += 1
                num1 = row

        file_data = io.BytesIO()
        workbook.save(file_data)
        file_data.seek(0)
        file_data.read()

        self.sudo().write({
            'data': base64.encodebytes(file_data.getvalue()),
            # 'name': 'employee_hourly_sale_report.xls',
            'name': 'informe_de_venta_por_hora_del_empleado.xls',
        })
        return {
            'type': 'ir.actions.act_url',
            'name': 'Employee Hourly Sale Report',
            'url': '/web/content/employee.hourly.sale/%s'
                   # '/data/employee_hourly_sale_report.xls?download=true' % self.id,
                   '/data/informe_de_venta_por_hora_del_empleado.xls?download=true' % self.id,
        }

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
