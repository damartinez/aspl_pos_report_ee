# -*- coding: utf-8 -*-
#################################################################################
# Author      : Acespritech Solutions Pvt. Ltd. (<www.acespritech.com>)
# Copyright(c): 2012-Present Acespritech Solutions Pvt. Ltd.
# All Rights Reserved.
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#################################################################################

from datetime import datetime

import pytz
from odoo import fields, models, api, _
from odoo.exceptions import ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT, DEFAULT_SERVER_DATETIME_FORMAT
from pytz import timezone


class WizardSalesDetails(models.TransientModel):
    _name = 'wizard.sales.details'
    _description = 'Wizard Sales Details'

    start_date = fields.Date(string="Start Date")
    end_date = fields.Date(string="End Date")
    report_type = fields.Selection([('thermal', 'Thermal'), ('pdf', 'PDF')],
                                   default='pdf', readonly=True, string="Report Type")
    user_ids = fields.Many2many('res.users',
                                'acespritech_pos_details_report_user_rel', 'user_id', 'wizard_id',
                                'Salespeople')
    proxy_ip = fields.Char(string="Proxy IP")
    only_summary = fields.Boolean("Only Summary")

    def print_sales_details(self):
        data = {'ids': self._ids,
                'form': self.read()[0],
                'model': 'wizard.sales.details'}
        return self.env.ref('aspl_pos_report_ee.report_sales_details_pdf').report_action(
            self, data=data)

    @api.onchange('start_date', 'end_date')
    def onchange_date(self):
        if self.start_date and self.end_date and self.start_date > self.end_date:
            raise ValidationError(_('End date should be greater than start date.'))

    def print_pos_sale_action(self):
        if self.start_date > self.end_date:
            raise ValidationError(_('End date should be greater than start date.'))
        return True

    def get_current_date_time(self):
        user_tz = self.env.user.tz or pytz.utc
        return {'date': datetime.now(timezone(user_tz)).date(),
                'time': datetime.now(timezone(user_tz)).strftime('%I:%M:%S %p')}

    def date_time_to_utc(self, state, end):
        tz = pytz.timezone(self.env.user.tz or pytz.utc)

        stat_date = datetime.strptime(state, DEFAULT_SERVER_DATETIME_FORMAT)
        end_date = datetime.strptime(end, DEFAULT_SERVER_DATETIME_FORMAT)

        stat_date = tz.localize(stat_date, is_dst=None).astimezone(pytz.utc)
        end_date = tz.localize(end_date, is_dst=None).astimezone(pytz.utc)

        stat_date.strftime(DEFAULT_SERVER_DATE_FORMAT + ' %H:%M:%S')
        end_date.strftime(DEFAULT_SERVER_DATE_FORMAT + ' %H:%M:%S')
        return stat_date, end_date

    def get_total_tax(self, user_id=None):
        start_date, end_date = self.date_time_to_utc(
            fields.Date.to_string(self.start_date) + ' 00:00:00',
            fields.Date.to_string(self.end_date) + ' 23:59:59')
        sql = """SELECT COALESCE(SUM(amount_tax), 0.0) AS total_tax
                                FROM pos_order
                                WHERE state != 'draft'
                                AND date_order >= '%s'
                                AND date_order <= '%s'
                                AND amount_total > 0
                                """ % (start_date, end_date)
        if not self.only_summary and user_id:
            sql += """AND pos_order.user_id = %s""" % user_id
        self.env.cr.execute(sql)
        return self.env.cr.dictfetchall()[0]['total_tax']

    def get_total_sales(self, user_id=None):
        start_date, end_date = self.date_time_to_utc(
            fields.Date.to_string(self.start_date) + ' 00:00:00',
            fields.Date.to_string(self.end_date) + ' 23:59:59')
        sql = """SELECT COALESCE(SUM(amount_total), 0.0) AS total_sales
                    FROM pos_order AS po
                    WHERE po.date_order >= '%s'
                    AND po.date_order <= '%s'
                    AND po.state NOT IN ('draft', 'cancel')
                    AND po.company_id = %s
                    """ % (start_date, end_date, self.env.user.company_id.id)
        if not self.only_summary and user_id:
            sql += """AND po.user_id = %s""" % user_id
        self._cr.execute(sql)
        return self._cr.dictfetchall()[0]['total_sales']

    def get_total_return_sales(self, user_id=None):
        start_date, end_date = self.date_time_to_utc(
            fields.Date.to_string(self.start_date) + ' 00:00:00',
            fields.Date.to_string(self.end_date) + ' 23:59:59')

        sql = """SELECT
                COALESCE(SUM(amount_total), 0.0) AS total
                FROM pos_order AS po
                WHERE po.amount_total < 0
                AND po.date_order >= '%s' AND po.date_order <= '%s'
                AND po. state != 'draft'
                AND po.company_id = %s
                """ % (start_date, end_date, self.env.user.company_id.id)
        if not self.only_summary and user_id:
            sql += """AND po.user_id = %s""" % user_id
        self._cr.execute(sql)
        return abs(self.env.cr.dictfetchall()[0]['total'])

    def get_currency_id(self):
        company_id = self.env['res.users'].browse([self._uid]).company_id
        return company_id.currency_id

    def get_total_discount(self, user_id=None):
        if self:
            start_date, end_date = self.date_time_to_utc(
                fields.Date.to_string(self.start_date) + ' 00:00:00',
                fields.Date.to_string(self.end_date) + ' 23:59:59')
            sql = """SELECT 
                        COALESCE(SUM((((pol.price_unit * pol.qty) * pol.discount) / 100)), 
                        0.0) AS discount
                        FROM pos_order AS po
                        INNER JOIN pos_order_line AS pol ON pol.order_id = po.id
                        AND po.state != 'draft'
                        AND po.date_order >= '%s'
                        AND po.date_order <= '%s'
                        AND pol.discount > 0 
                        AND pol.price_subtotal > 0 """ % (start_date, end_date)
            if user_id:
                sql += """AND po.user_id = %s """ % user_id
            self.env.cr.execute(sql)
            return self.env.cr.dictfetchall()[0]['discount']
        return 0.0

    def get_gross_total(self):
        gross_total = 0.0
        company_id = self.env.user.company_id.id
        domain = [
            ('state', '!=', 'draft'),
            ('company_id', '=', company_id),
            ('date_order' ,'>=',  self.start_date),
            ('date_order', '<=', self.end_date)
        ]
        pos_order_obj = self.env['pos.order'].search(domain)
        for order in pos_order_obj:
            for line in order.lines:
                gross_total += line.price_subtotal -  (line.qty *line.product_id.standard_price)
        return gross_total

    # def get_gross_profit(self):
    #     gross_profit_total = 0.0
    #     start_date, end_date = self.date_time_to_utc(
    #             fields.Date.to_string(self.start_date) + ' 00:00:00',
    #             fields.Date.to_string(self.end_date) + ' 23:59:59')
    #     company_id = self.env.user.company_id.id
    #     domain = [
    #         ('state', '!=', 'draft'),
    #         ('company_id', '=', company_id),
    #         ('date_order', '>=', start_date),
    #         ('date_order', '<=', end_date)
    #     ]
    #     pos_order_obj = self.env['pos.order'].search(domain)
    #     for order in pos_order_obj:
    #         for line in order.lines:
    #             gross_profit_total += ((line.price_subtotal - (line.qty * line.product_id.standard_price)) / line.price_subtotal)*100
    #     return gross_profit_total

    def get_net_gross_total(self, user_lst=None):
        net_profit_total = 0.0
        start_date, end_date = self.date_time_to_utc(
                fields.Date.to_string(self.start_date) + ' 00:00:00',
                fields.Date.to_string(self.end_date) + ' 23:59:59')
        company_id = self.env.user.company_id.id
        domain = [
            ('state', '!=', 'draft'),
            ('company_id', '=', company_id),
            ('date_order', '>=', start_date),
            ('date_order', '<=', end_date)
        ]
        pos_order_obj = self.env['pos.order'].search(domain)
        for order in pos_order_obj:
            for line in order.lines:
                discount = (((line.price_unit * line.qty) * line.discount) / 100)
                net_profit_total += ((line.price_subtotal_incl - ((line.qty * line.product_id.standard_price) + discount)) / line.price_subtotal_incl)*100
        return net_profit_total

    def get_all_users(self):
        user_ids = self.env['res.users'].search([])
        return user_ids.ids

    def get_pos_ids(self, start_date, end_date, user_ids, company_id):
        pos_ids = self.env['pos.order'].search(
            [('date_order', '>=', str(self.start_date) + ' 00:00:00'),
             ('date_order', '<=', str(self.end_date) + ' 23:59:59'),
             ('state', 'in', ['paid', 'invoiced', 'done']),
             ('user_id', 'in', user_ids), ('company_id', '=', company_id)])
        return pos_ids

    def get_product_category(self,user_lst=None):
        if not user_lst:
            user_ids = [user.id for user in self.user_ids] or self.get_all_users()
        else:
            user_ids = user_lst
        company_id = self.env['res.users'].browse([self._uid]).company_id.id
        pos_ids = self.get_pos_ids(self.start_date, self.end_date, user_ids, company_id)
        category_data = []
        if pos_ids:
            sql = """SELECT pc.name AS category, SUM(pol.price_subtotal_incl) AS total 
                        FROM pos_order as po
                        LEFT JOIN pos_order_line AS pol on pol.order_id = po.id
                        INNER JOIN product_product AS pp on pp.id = pol.product_id
                        INNER JOIN product_template AS pt on pt.id = pp.product_tmpl_id
                        LEFT JOIN pos_category AS pc on pc.id = pt.pos_categ_id
                        WHERE po.session_id in (%s)
                        AND po.state != 'draft'
                        GROUP BY pc.name, pc.id 
                    """ % ', '.join([str(each.session_id.id) for each in pos_ids])
            self.env.cr.execute(sql)
            category_data = self.env.cr.dictfetchall()
        print("\n\n\n\n----->>",)
        return category_data

    def get_product_name(self, category_id):
        if category_id:
            category_name = self.env['pos.category'].browse([category_id]).name
            return category_name

    def get_product_cate_total(self, user_lst=None):
        balance_end_real = 0.0
        if not user_lst:
            user_ids = [user.id for user in self.user_ids] or self.get_all_users()
        else:
            user_ids = user_lst
        company_id = self.env['res.users'].browse([self._uid]).company_id.id
        pos_ids = self.get_pos_ids(self.start_date, self.end_date, user_ids, company_id)
        for order in pos_ids:
            for line in order.lines:
                balance_end_real += (line.qty * line.price_unit)
        return balance_end_real

    def get_payments(self, user_lst=None):
        statement_line_obj = self.env["account.bank.statement.line"]
        if not user_lst:
            user_ids = [user.id for user in self.user_ids] or self.get_all_users()
        else:
            user_ids = user_lst
        company_id = self.env['res.users'].browse([self._uid]).company_id.id
        pos_ids = self.get_pos_ids(self.start_date, self.end_date, user_ids, company_id)
        st_line_ids = statement_line_obj.search([('statement_id', 'in', pos_ids.ids)])
        if st_line_ids:
            statement_list = []
            for line in st_line_ids:
                statement_list.append(line['id'])
            self._cr.execute(
                "select aj.name,sum(amount) "
                "from account_bank_statement_line as absl,"
                "account_bank_statement as abs,account_journal as aj "
                "where absl.statement_id = abs.id and abs.journal_id = aj.id and absl.id IN %s "
                "group by aj.name ", (tuple(statement_list),))

            data = self._cr.dictfetchall()
            return data

    def get_user_wise_data(self):
        result = {}
        for user in self.user_ids:
            result.update({
                user.name: {
                    'total_discount': self.get_total_discount(user.id),
                    'total_sales': self.get_total_sales(user.id),
                    'taxes': self.get_total_tax(user.id),
                    'gross_total': self.get_gross_total(),
                    'gross_profit': self.get_gross_profit(),
                    'net_gross': self.get_net_gross_total(user.id),
                    'payment': self.get_payments([user.id]),
                    'product_category': self.get_product_category([user.id]),
                    'prod_categ_total': self.get_product_cate_total([user.id]),
                }
            })
        return result

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
