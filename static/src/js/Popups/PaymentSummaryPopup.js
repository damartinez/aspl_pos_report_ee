odoo.define('aspl_pos_report_ee.PaymentSummaryPopup', function(require) {
    'use strict';

    const { useState, useRef } = owl.hooks;
    const AbstractAwaitablePopup = require('point_of_sale.AbstractAwaitablePopup');
    const Registries = require('point_of_sale.Registries');

    class PaymentSummaryPopup extends AbstractAwaitablePopup {
        constructor() {
            super(...arguments);
            if (this.env.pos.config.payment_current_month_date){
                var first_date_of_month = moment().startOf('month').format('YYYY-MM-DD');
                var today_date = moment().format('YYYY-MM-DD');
            }
            this.state = useState({
                StartDate: first_date_of_month || false,
                EndDate: today_date || false,
                StartDateBlank: false,
                EndDateBlank: false,
                PaymentSelectData: "sales_person",
            });
            this.start_date = useRef('payment_start_date');
        }
        getPayload() {
            return {
                StartDate: this.state.StartDate,
                EndDate: this.state.EndDate,
                CurrentSession: this.state.CurrentReport,
                PaymentSelectData: this.state.PaymentSelectData,
            };
        }
        PaymentSelectDataCheck(event){
            this.state.PaymentSelectData = event
        }
        CurrentSessionCheck(){
            this.state.CurrentReport = !this.state.CurrentReport
        }
        async confirm() {
            if (!this.state.CurrentReport){
                if (this.state.StartDate == ""){
                    this.state.StartDateBlank = true;
                }
                if (this.state.EndDate == ""){
                    this.state.EndDateBlank = true;
                }
                if (this.state.StartDateBlank || this.state.EndDateBlank){
                    return
                } else if(this.state.StartDate > this.state.EndDate){
                    $('#lbl_set_available').html("Start date should not be greater than end date !");
                    return
                } else{
                    this.props.resolve({ confirmed: true, payload: await this.getPayload() });
                    this.trigger('close-popup');
                }
            } else{
                this.props.resolve({ confirmed: true, payload: await this.getPayload() });
                this.trigger('close-popup');
            }
        }
        cancel() {
            this.trigger('close-popup');
        }
    }
    PaymentSummaryPopup.template = 'PaymentSummaryPopup';
    PaymentSummaryPopup.defaultProps = {
        confirmText: 'Print',
        cancelText: 'Cancel',
        title: '',
        body: '',
    };

    Registries.Component.add(PaymentSummaryPopup);

    return PaymentSummaryPopup;
});
