odoo.define('aspl_pos_report_ee.AuditReportButton', function(require) {
    'use strict';

    const PosComponent = require('point_of_sale.PosComponent');
    const ProductScreen = require('point_of_sale.ProductScreen');
    const { useListener } = require('web.custom_hooks');
    const Registries = require('point_of_sale.Registries');

    class AuditReportButton extends PosComponent {
        constructor() {
            super(...arguments);
            useListener('click', this.onClick);
        }
        async onClick() {
            const { confirmed, payload } = await this.showPopup('AuditReportPopup', {
                title: this.env._t('Audit Report'),
            });
        }
    }
    AuditReportButton.template = 'AuditReportButton';

    ProductScreen.addControlButton({
        component: AuditReportButton,
        condition: function() {
            return this.env.pos.config.print_audit_report;
        },
    });

    Registries.Component.add(AuditReportButton);

    return AuditReportButton;

});
