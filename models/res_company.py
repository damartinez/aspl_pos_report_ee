#################################################################################
# Author      : Acespritech Solutions Pvt. Ltd. (<www.acespritech.com>)
# Copyright(c): 2012-Present Acespritech Solutions Pvt. Ltd.
# All Rights Reserved.
#
# This program is copyright property of the author mentioned above.
# You can`t redistribute it and/or modify it.
#
#################################################################################
from odoo import api, fields, models


class ProductProduct(models.Model):
    _inherit = 'product.product'

    @api.model
    def get_product_list(self, domain, fields, order):
        return self.env['product.product'].search_read(domain=domain, fields=fields)

# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
